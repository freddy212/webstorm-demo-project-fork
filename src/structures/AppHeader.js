import React, {Component} from 'react';
import AppBar from "material-ui/AppBar";
import SettingsDrawer from '../components/SettingsDrawer';

class AppHeader extends Component {

    constructor(props) {
        super(props);
        this.openMenu           = this.openMenu.bind(this);
        this.requestMenuOpen    = this.requestMenuOpen.bind(this);
        this.state              = { menuOpen: false };
    }

    openMenu() {
        this.setState({menuOpen: true});
    }

    requestMenuOpen(menuOpen=false){
        this.setState({menuOpen});
    }

    render() {
        return (
            <div>
                <AppBar title="Welcome to React" iconClassNameRight="muidocs-icon-navigation-expand-more" onClick={this.openMenu}/>
                <SettingsDrawer open={this.state.menuOpen} onRequestChange={this.requestMenuOpen}/>
            </div>
        );
    }

}

export default AppHeader;