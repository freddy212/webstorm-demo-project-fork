import React, {Component} from 'react';
import {
    DropDownMenu,
    IconButton,
    IconMenu,
    MenuItem,
    RaisedButton,
    Toolbar,
    ToolbarGroup,
    ToolbarSeparator
} from "material-ui";
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';


class GeneralToolBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: 3,
        };
    }

    handleChange = (event, index, value) => this.setState({value});

    render() {
        return (
            <Toolbar>
                <ToolbarGroup firstChild={true}>
                    <DropDownMenu value={this.state.value} onChange={this.handleChange}>
                        <MenuItem value={1} primaryText="All " />
                        <MenuItem value={2} primaryText="All Voice" />
                        <MenuItem value={3} primaryText="All Text" />
                        <MenuItem value={4} primaryText="Complete Voice" />
                        <MenuItem value={5} primaryText="Complete Text" />
                        <MenuItem value={6} primaryText="Active Voice" />
                        <MenuItem value={7} primaryText="Active Text" />
                    </DropDownMenu>
                </ToolbarGroup>
                <ToolbarGroup>
                    <ToolbarSeparator />
                    <RaisedButton label="Create Data Output" primary={true} />
                </ToolbarGroup>
            </Toolbar>
        );
    }
}

export default GeneralToolBar;