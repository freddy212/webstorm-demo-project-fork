import React, { Component } from 'react';
import Drawer from "material-ui/Drawer";
import MenuItem from "material-ui/MenuItem";

class SettingsDrawer extends Component {
    render() {
        return (
            <Drawer open={this.props.open}
                    docked={false}
                    width={200}
                    onRequestChange={this.props.onRequestChange}>
                <MenuItem>YOOO</MenuItem>
                <MenuItem>NOOOW</MenuItem>
            </Drawer>
        );
    }
}

export default SettingsDrawer;