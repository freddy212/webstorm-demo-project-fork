import React, {Component} from 'react';
import './Main.css';
import RaisedButton from "material-ui/RaisedButton";
import GeneralToolBar from "../components/GeneralToolBar";

const style = {
    margin: 12,
};

class MainView extends Component {
    render() {
        return (
            <div className="App">
                <GeneralToolBar />
                <p className="App-intro">
                    <p>NICE</p>
                    <RaisedButton label="YOU KNOW WHAT" style={style}/>
                </p>
            </div>
        );
    }
}

export default MainView;

